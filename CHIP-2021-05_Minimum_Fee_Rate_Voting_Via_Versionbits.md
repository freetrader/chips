# CHIP-2021-05 Minimum Fee Rate Voting Via Versionbits

        Title: Minimum Fee Rate Voting Via Versionbits
        Maintainer: freetrader
        Authors: freetrader (freetrader@tuta.io)
        Type: Technical
        Is consensus change: No
        Status: DRAFT
        Version: 0.2
        Initial Publication Date: 2021-05-30
        Last Revision Date: 2021-06-06

## Discussions

- https://bitcoincashresearch.org/t/chip-2021-05-minimum-fee-rate-voting-via-versionbits/465

- https://www.reddit.com/r/btc/comments/no0atu/chip202105_minimum_fee_rate_voting_via_versionbits/


## Summary

We propose a minimum relay fee adjustment mechanism driven by miner voting in
block header version bits 11 and 12. This will allow hashpower to express a
fee adjustment preference: increase, decrease or no change to the relay fee.

The votes would be evaluated at regular intervals by full nodes and other
software such as SPV wallets.
A minimum relay fee change would happen if more than 50% of blocks within the
last 1,008 voted in its favor.
The votes would be evaluated approximately once a week, or more exactly:
whenever the block height is an integer multiple of 1,008.

Fee adjustments would be made in steps of 25% of the prior fee rate.


## Deployment

Deployment of this specification is proposed for the May 2022 upgrade.

Applications should only be lowering their minimum relay fee rates
below 1000 sat/kB once they have implemented this specification to compute
the new minimum fee rate or made some arrangement to reliably obtain this
data from sources they can trust.


## Motivation and Benefits

Transaction fee adjustments have occurred several times in Bitcoin's life.
Bitcoin Cash (BCH) has operated at a flat rate of 1000 sat/kB for the
minimum relay fee which is user-configurable in popular full node software.

In a scenario where the BCH price rises substantially, this minimum fee rate
could lead to significantly [high transaction fees](#appendix-a-fee-projections)
which could in turn make the BCH network comparatively uncompetitive and
thereby interfere with economic activity and drive users away to other
payment systems.

While manual changes to the minimum relay fee rate are possible, they may be
difficult to coordinate and could fail in a way that leads to a fragmented
fee policy. Such a state would be detrimental to transaction relay and
confirmation reliability. It would likely damage the reputation and
network effect of Bitcoin Cash.

The miner voting-based control proposed in this CHIP would alleviate this
coordination burden in a publicly accountable and predictable way, ensuring
that the Bitcoin Cash miners can react autonomously to protect the network
effect against competing payment systems without needing developers or
end users to further intercede.

This should help to avoid a high-fees situation such as the cryptocurrency
community has seen play out repeatedly in other coins (e.g. BTC, ETH, XMR,
LTC, DOGE) where fees have risen to levels that harm the coins' uses outside
of speculation.


## Limitation of Scope

Considered to be **within the scope** of this CHIP are:

- Miner-voting based control of the minimum relay fee strictly between a floor
  of 1 sat/kB and a ceiling of 1000 sat/kB

Considered to be **outside of the scope** of this CHIP are:

- An initial minimum relay fee reduction upon first activation of the CHIP.
  This could be moved into scope if there was sufficient agreement on a need
  for it, prior to finalization of this CHIP.
- Fee rates below current configurable minimum of 0.001 sat/byte
  (ie. below 1 sat/kB). These would require more involved changes on the side
  of implementations.
- Increasing the divisibility of Bitcoin Cash (aka "fractional satoshis").
  This is not needed for fee rates considered within the scope of this CHIP
  and would necessitate changes to how monetary amounts on the BCH blockchain
  are interpreted or even represented (depending on how fractional satoshis
  are implemented).
- Additional rate limitation mechanisms of transaction relay or mempool admission.
  This is considered a separate policy matter.
- Additional transaction relay or mining (de)prioritization mechanisms such as
  coin-days-destroyed or similar. This is considered a separate policy matter.
- The minimum fee rate exacted by miners to include transactions in blocks they
  mine. This is already a separate parameter in most implementations and remains
  under the full control of each miner, even though this CHIP *recommends* that
  miners use the same value as the computed minimum relay fee under this CHIP.

In summary, this CHIP is only about relay fee control and *potentially*,
a one-time drop to a lower fee rate when the control process is started.


## Background

Network nodes on Bitcoin Cash currently offer policy settings which
regulate the minimum fee rates (ultimately measured in satoshis per byte)
which need be paid by transactions in order to be admitted to a
node's memory pool and relayed to other nodes.

Most nodes offer miners additional control of the minimum fee rate to
further restrict which transactions the miner will include in a block,
but this is separate from the issue of which transactions are relayed
across the network.

Although every node is free to set these limits independently, and miners
are always free to include any valid transactions into a block - even
those paying zero fees - a common policy around the minimum fees has evolved
in order to avoid problems with submitting, forwarding and confirming
transactions.

The policy has solidified around a minimum fee rate of 1000 sat/kB
which has generally been considered sufficient (note the lack of
miner initiatives to raise this threshold as the overwhelming proportion
of hashing income is still from the block rewards via newly minted coins).

The common minimum fee policy has made it simple and convenient for
applications to calculate the fees required for a transaction on the
Bitcoin Cash network (unlike on BTC). The common policy also strengthens
the network's resistance against certain kinds of double spending attacks.

Coordination of policy changes is difficult but necessary in a decentralized
system such as Bitcoin Cash whose market price is subject to large
fluctuations over at least its early lifetime.
The benefits to the network of a consistent relay fee policy are great
enough to justify a protocol for such fee changes.


## Technical Specification

### Terms and Conventions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in RFC 2119.

Generally, the term "fee" may be loosely used in this document to
refer to fee rates.

The abbreviation 'sat' is used for the smallest unit of Bitcoin Cash,
the satoshi, equal to 0.00000001 BCH.

Fee rates in this specification are expressed in satoshis/kB.
Most implementations today represent these as fee rates internally as
integer values with a minimum value of 1 sat/kB.
This specification does not propose going below that minimum.

In mathematical formulas, operators like +, -, /, *, and ^ (denoting
exponentiation) are used conventionally.


### Formal Requirements

#### REQ-MINFEE-VOTING-FLOOR

The minimum relay fee rate SHALL NOT be decreased below 1 sat/kB.

Note 1: Going below 1 sat/kB is out of scope of this proposal.


#### REQ-MINFEE-VOTING-CEILING

The minimum relay fee rate SHALL NOT be raised above below 1000 sat/kB.

Note 1: Not exceeding this level means that current applications which
        calculate based on 1 sat/byte, will not encounter transmission
        problems under this proposal.


#### REQ-MINFEE-VOTING-BITS-RANGE

Bits 11 and 12 of the block header nVersion field (counting from LSB=0)
SHALL be used as fee adjustment bits (FABs).

Note 1: The FABs will be allocated permanently (at least as long as this
        CHIP is in effect). Applications implementing BIPs 8,9,135 shall
        mask FABs out if considering other versionbits signaling.


#### REQ-MINFEE-VOTING-BIT-VALUES

The meaning of the fee adjustment bits SHALL be as follows:

| Bit 12 | Bit 11 | Binary | Decimal |    Meaning of bits     |
|--------|--------|--------|---------|------------------------|
|    0   |    0   |   00   |    0    | Vote for no fee change |
|    0   |    1   |   01   |    1    | Vote for fee decrease  |
|    1   |    0   |   10   |    2    | Vote for fee increase  |
|    1   |    1   |   11   |    3    | Reserved (ref. Note 1) |

Note 1: "Reserved" SHALL be treated same as "Vote for No Change"
        in this specification.

Note 2: Implementations which generate blocks SHOULD default
        to a value of '0' (no fee change) for the FABs and require
        operator action to change this to another value.


#### REQ-MINFEE-VOTING-PERIOD

The fee voting period SHALL be 1,008 (one thousand and eight) blocks,
and vote tallying/evaluation SHALL take place whenever the block height
reaches an integer multiple of 1,008.

The FAB votes in the past 1,008 blocks (starting from the most recently
received one whose height is a multiple of 1,008) will be tallied and the
threshold set in
[REQ-MINFEE-VOTING-THRESHOLD](#req-minfee-voting-threshold)
will be used to decide whether a change to the effective minimum fee must
be made or not.

Note 1: The voting period is chosen to correspond to approximately
        one week (144 blocks/day * 7 days).

Note 2: The magnitude of the fee change is calculated as per
        [REQ-MINFEE-VOTING-ADJUSTMENT-MAGNITUDE](#req-minfee-voting-adjustment-magnitude)
        (see below).

#### REQ-MINFEE-VOTING-THRESHOLD

An increase or decrease of the minimum fee rate SHALL be made
if more than 50% of the FAB votes (505 or more votes) over the last
voting period are in favor of a change in the respective direction.

Pseudo-code:

```
ALGORITHM evaluate_and_update_fees is
    INPUT:  current_relay_fee_rate,          ; the current relay fee rate in sat / kB
            chaintip_height                  ; block height at chain tip
            block_headers_vector             ; vector of block headers
    OUTPUT: new_relay_fee_rate               ; the new minimum relay fee rate
    PRECONDITION:  (1 <= current_relay_fee_rate <= 1000) AND (size(block_headers_vector) >= 1008)
    POSTCONDITION: 1 <= new_relay_fee_rate <= 1000
    CONSTANTS: voting_period = 1008          ; number of blocks
               voting_threshold = 505        ; threshold for change: > 50% of voting_period
               vote_to_decrease_fee = 0x01   ; FAB value for fee DECREASE
               vote_to_increase_fee = 0x02   ; FAB value for fee INCREASE

    IF chaintip_height MODULO voting_period == 0 THEN
        new_relay_fee_rate ← current_relay_fee_rate  ; default is no change
        ; evaluate the votes
        tally_to_decrease ← 0
        tally_to_increase ← 0
        index ← 0
        LOOP WHILE index < voting_period
            ; get bits of a header, mask out all bits except 12,11 with bitwise AND,
            ; and shift the result right by 11 bits
            ballot = shift_right( block_headers_vector[chaintip_height - index].nVersion & 0x1800, 11 )
            IF ballot == vote_to_decrease_fee THEN
                tally_to_decrease ← tally_to_decrease + 1
            ELSE IF ballot == vote_to_increase_fee THEN
                tally_to_increase ← tally_to_increase + 1
            END IF
            index ← index + 1
        END LOOP
        IF tally_to_decrease >= voting_threshold THEN
            new_relay_fee_rate ← compute_decreased_fee(current_relay_fee_rate)
        ELSEIF tally_to_increase >= voting_threshold THEN
            new_relay_fee_rate ← compute_increased_fee(current_relay_fee_rate)
        END IF
    END IF

    RETURN new_relay_fee_rate
```
Note 1: If there is a tie of exactly 504-to-504 between votes for
        increase and decrease, no change is made to the fee.

#### REQ-MINFEE-VOTING-ADJUSTMENT-MAGNITUDE

The delta to the current fee SHALL be computed as 25% of the current fee
or at least 1 sat/kB, whichever is greater, with the resulting increased or
decreased fee clamped to the integer range [1, 1000].

Pseudo-code:

```
ALGORITHM compute_decreased_fee is
    INPUT:  old_fee_rate                     ; the current relay fee rate in sat / kB
    OUTPUT: new_fee_rate                     ; the new relay fee rate
    PRECONDITION:  1 <= old_fee_rate <= 1000
    POSTCONDITION: 1 <= new_fee_rate <= 1000
    CONSTANTS: adjustment_percentage = 25    ; factor used when adjusting fees: 25%
               fee_floor = 1                 ; lowest result is 1 sat/kB

    IF old_fee_rate == fee_floor THEN
        RETURN fee_floor
    ELSE
        decrement ← max(1, (old_fee_rate * adjustment_percentage) / 100)
        RETURN max(fee_floor, old_fee_rate - decrement)
    END IF
```

```
ALGORITHM compute_increased_fee is
    INPUT:  old_fee_rate                     ; the current relay fee rate in sat / kB
    OUTPUT: new_fee_rate                     ; the new relay fee rate
    PRECONDITION:  1 <= old_fee_rate <= 1000
    POSTCONDITION: 1 <= new_fee_rate <= 1000
    CONSTANTS: adjustment_percentage = 25    ; factor used when adjusting fees: 25%
               fee_ceiling = 1000            ; highest result is 1000 sat/kB

    IF old_fee_rate == fee_ceiling THEN
        RETURN fee_ceiling
    ELSE
        increment ← max(1, (old_fee_rate * adjustment_percentage) / 100)
        RETURN min(fee_ceiling, old_fee_rate + increment)
    END IF
```

Note 1: Since the fee rate is assumed to be accurate down to 1 sat/kB,
        any change must be at least of this magnitude.


#### REQ-MINFEE-ACCURACY

The minimum relay fee rate SHALL be maintained with an accuracy of 1 sat/kB.

Note 1: Implementations are encouraged to make use of integer arithmetic
        in their calculations.
        Floating point arithmetic may be used as long as the results are
        strictly checked to arrive at the same values as their integer
        counterparts.


#### REQ-MINFEE-VOTING-EFFECTIVENESS

Once an updated minimum relay fee rate has been determined based
on evaluation of the votes, this new rate SHALL be considered
*effective immediately* to control relay of transactions.

Note 1: Applications should no longer relay transactions received thereafter
        which do not meet the new minimum relay fee rate.

Note 2: Applications should apply this new fee rate to any transactions which
        they have not yet broadcast onto the network, subject to their
        discretion (they may of course opt for generating transactions
        with a slightly higher fee rate than the minimum set for the
        network, or do so for a time period after an adjustment, if they wish
        to be on the safer side).


#### REQ-MINFEE-ACTIVATION-FLUSH

If this CHIP is deployed as part of a network upgrade activated by
MTP, then the first evaluation of votes SHALL take place not on the
first integral multiple of 1,008, but on the second one after the
block which marked the MTP activation.

Note 1: This allows a full voting period to start and finish after
        an MTP-based network upgrade.

Note 2: The alternative is to schedule the activation of this CHIP
        independently based on block height so that it activates
        at some multiple of 1,008, perhaps with a nonzero modulus
        in order to pick an arbitrary block height.
        However, this should still allow a full voting period to
        pass before fee changes are made based on votes.


#### REQ-MINFEE-ACTIVATION-ANCHOR

The first calculation of the new minimum relay fee SHALL be
based on a current relay fee value of 1000 sat/kB, regardless of
any older software configuration settings, unless the operator
has disabled the new minimum relay fee calculation (i.e. disabled
this CHIP in his software).

Note 1: Existing software typically has a configuration parameter
        for the minimum relay fee rate. For the new policy to take
        effect in a coordinated manner, software implementing
        must start from a common "anchor" value, otherwise fee
        calculations would produce inconsistent results across
        the network, defeating the objective of this proposal.


## Rationale and Commentary on Requirements / Design Decisions

### 1. Miner voting as a basis for deciding relay fee changes

Using blockchain data allows all applications to quite easily stay
informed of any fee changes. Any application that uses full nodes
can determine the current fee rate based on the starting point
(deployment of this CHIP) and block header data.
Fee rate data can also be checkpointed for blocks that have been
sufficiently confirmed, to reduce the need to download much data.

Miners are incentivized to care about the long term health of the
network, since block rewards reduce over time and need to be replaced
by transaction fees in the long run.
Thus, ensuring that Bitcoin Cash remains competitive with other
payment methods is in the interest of miners.
Keeping fees competitive attracts paying traffic which ultimately
sustains network security.

### 2. Using percentage-based adjustments

Linear adjustments (+/- N sat/kB) were considered, but while even
easier to understand and to implement, they are not as reactive
unless big quanta are used. This would result in a loss of relative
adjustment precision as fee rates decrease.

Percentage-based adjustment allows relatively quick decrease of the
fee from its initial setting in case of external pressure, and works
well at the lower end of the fee rate spectrum where finer control
is assumed to be desirable.

### 3. Choice of versionbits

Draft BIP 661 [4] defined a range of sixteen bits from the block header
nVersion field, starting from 13 and ending at 28 inclusive (0x1fffe000).
These were reserved for general use and removed from BIP 9 [1] and
BIP 8 [2] specifications.
In reality, these bits are probably only used for version-rolling
(see BIP 310 [3]).

This leaves bits 0-12 for BIP 8 / BIP 9, but these soft-fork bits are
not currently used on Bitcoin Cash, and even if someone considered using
versionbits to vote for consensus change activations on BCH in future,
there are still 11 bits available.

Therefore it is proposed to allocate the two bits from the high end of
that range, bits 11 and 12, permanently for relay fee adjustment voting.

### 4. Choice of voting period, threshold, anchor and magnitude

The voting period was chosen in combination with the adjustment factors
to facilitate a speedy rate of change -- approximately a halving (or doubling) of
the fee rate within less than a month, while requiring a majority
of hashpower to effect a change at any time. The threshold has been
chosen such that it should remain possible for an honest mining majority to
defend against hostile voting.

Should the voting period prove a little too frequent, it can always be
extended in a future network upgrade to provide more fee stability.
Such future fine-tuning is also possible for the percentage, and of
course the entire policy control mechanism proposed by this CHIP can be
deactivated or replaced with something completely different in a future
upgrade.

### 5. Lack of initial adjustment ("drop") on activation

An initial drop upon activation from 1000 sat/kB down to a
lower value was considered, but since there is no urgent pressure
for such a fee drop at the time of writing, it is proposed to launch with
the anchor value equal to the existing fee rate of 1000 sat/kB and let
the regular voting mechanism handle future fee movement.

This simplifies the proposal, but as long as this CHIP is still under review,
the author reserves to the right to add such an initial fee decrease
requirement, esp. if external scenarios result in a dramatic short-term
rise of the BCH price.

### 6. Deployment in May 2022

While deployment could, in an emergency situation, be accelerated, this
CHIP should be deployed in a coordinated manner by a majority of relaying
(read: fully-validating) nodes.

The next planned network upgrade is May 2022, which is a good opportunity
to bring such a longer term fee regulation mechanism into play without
disrupting other ongoing work.


## Tradeoffs

All fee adjustment proposals will make some tradeoffs, and there are many
alternatives (see [Evaluation of Alternatives](#evaluation-of-alternatives)).

1. Slightly more complex to implement than linear adjustments or a table of fees.

2. Risk of miners manipulating the minimum relay fees - mitigated by
   a sizable voting window of one week; if some unknown miners suddenly
   vote for fee changes without there being a visible economic reason,
   this will not go unnoticed.

3. Biggest fee changes under this proposal are potentially smaller than
   under some "absolute fee value" voting proposals or strictly one-time
   fee changes. This is mitigated by choosing the parameter to still
   allow significant changes within relatively short time (e.g one month).


## Current Implementations

[TODO]

There is no implementation of the full algorithm in node software yet.
This proposal is in an early Draft stage seeking further comment from
stakeholders.

A Python implementation exists for calculation of the fee changes.
It has been used to generate the graphical figures, test vectors and
some data for some tables in this document.

The pseudo-code algorithms for computing the increased/decreased fees have
been derived from this Python implementation as well.


## Implementation Costs and Risk Mitigations

The following implementation costs and risks have been assessed.

### Node Upgrade Costs

This proposal only affects policy - there is no risk of any nodes
falling out of consensus. Nor should this change negatively impact
scalability or feasible implementation on certain types of nodes
(pruned or "fast-synced").

All nodes can be expected to have at least the block headers for
the active chain at their disposal, and thus have the necessary
voting information to implement the relay fee adjustment algorithm.
Implementing and testing the algorithm should not cost more than a
few days of effort. Test vectors would be provided for unit testing
the calculations. Some integration and system testing on test
networks would be needed to ensure that relay fees are consistent
among node implementations.

There is some additional code needed to activate this change smoothly
in the context of a network upgrade (ensuring that a full voting window
has been allowed to pass before making fee adjustments based on the
FABs). Such code can be removed up as part of regular maintenance
shortly after the upgrade, once the voting-based fee adjustments
have taken place for the first time.

Nodes may opt to implement additional interfaces to:

- Set the fee adjustment preference (vote) for future blocks.
  Configuring which versionbits to set is usually left to pool software.
  However, it may be necessary to implement such interfaces for
  testing purposes anyway.
  A more convenient interface would let the operator configure
  a target fee value and the node software would use that to
  determine whether it should vote for an increase or decrease.

- Inspect tallied votes (increase, decrease, or no change) during a
  voting period. This will also help to check consistency among
  nodes during testing ahead of deployment.

- Disable or override this automatic adjustment mechanism
  (falling back to using operator-configured relay fee settings)
  This may be dangerous as operators might misunderstand that they
  can use this to somehow "opt out" of the network wide policy
  changes.

### Wallet and Library Upgrade Costs

Wallets and libraries which today construct transactions based on the
flat fee assumption of 1000 sat/kB, would continue to work although
they would be overpaying in case the minimum relay fees were to be
decreased.

However, this is at least safe - their transactions would continue
to be accepted and relayed by network nodes, and they could implement
this change at any time before or after it is deployed in full nodes.

SPV wallets should be modified to calculate the applicable relay
fee based on block header votes after some known historical starting
conditions (e.g. the 1000 sat/kB fee level in effect at the time
this CHIP is deployed in an upgrade).

Note: Deep reorganizations are prevented by the 10-block automatic
      finalization which is widely used on the network.
      The relay fee in effect at the last finalized block could be
      saved as a baseline for future fee calculations, together with
      a running tally of votes up to that finalized block. Doing so
      could reduce the amount of work that a node or application
      needs to do when the voting period lapses and the votes need
      to be finally evaluated.

Light wallets operating off their own backend servers would need to
obtain the current relay fee from them. At least some fully-validating
nodes provide this relay fee information, e.g. via the `relayfee`
field of the `getnetworkinfo` RPC command.

### Risk of Choosing an Excessive Fee Rates Granularity

The fewer steps there are in between the floor and the ceiling, the
higher the likelihood that the fee rate determined at some time,
may not be a good enough match for some miners to remain profitable.
The lower minimum fee would induce more transactions which pay less,
A miner might be forced to institute their own, slightly higher,
mining fee limit. Miners who are underpaid and need to filter
relayed transactions in this way, would be voting for a fee
increase under this CHIP.

The mitigation is to require only the majority of votes to
effect a fee change, to prevent a minority from blocking fee
rate change in the direction that would provide relief.


## Ongoing Costs and Risk Mitigations

### Maintenance Costs

There would be minor maintenance costs arising from the new code that
must be added in nodes, wallets and perhaps some libraries to track the
evolving fee rates.

This would be simple and contained code though - probably not more than
a few hundred lines to maintain, including test code.

### Risk of Miners Rejecting The Responsibility

Miners may not want this responsibility and be afraid that they would
have to micro-manage fee policy and be under a new, possibly persistent
pressure from users and businesses.

This can be mitigated by making their target fee level of choice
configurable so that they do not have to micro-manage this feature -
the node software could determine which direction to vote based
on a fee preference they would not need to modify often.

### Risk of fee floor being too high

The fee floor of 1 sat/kB could not be dropped very easily.
One could increase the unit of the denominator, e.g. to 1 sat/10kB, or
1 sat/100kB, or 1 sat/MB. This leads to a loss of precision with which
transaction fees can be set, for regular small-sized transactions.

The mitigation would be to further subdivide the satoshi unit to
represent the smaller values needed by smaller fee amounts.

There are proposals on how to do this, but BCH is so far away in terms
of price for this to be an urgent problem that we can consider the risk low
for at least several years, even under optimistic growth assumptions.

In a hyperinflation scenario, the fees should actually not remain
so low, but rise in fiat values to compensate for loss of purchasing
power. So we may never actually need to go below 1 sat/kB to retain
the customary "fees below $0.01 per transaction" if there is some abrupt
event and we hit or exceed the $10M USD/BCH mark.

### Risk of fee ceiling being too low

This risk is seen as very low as the fee floor of 1000 sat/kB was sufficient
during BCH all-time low which was several factors lower than current
price. The existing minimum fee has thus proved itself down to very
price levels (even under 100 USD/BCH). There is little reason to think
that BCH price would fall so substantially below that ATL in the future
to require a raising of the relay fee ceiling.

Even if the relay fee ceiling is not raised, miners can still exclude
transactions they deem uneconomical to mine in order to protect
themselves against such events.

This risk could be mitigated by rolling out a fee ceiling increase,
either coordinated at some block height or as an emergency increase
triggered by hitting the existing ceiling or being stuck on it for
a specified time.
Raising the ceiling specified in this CHIP is not problematic as long
as the current fee is at the ceiling at the time the new ceiling goes
into operation.

### Risk of attacks by hostile miners

Hostile hash rate, if it obtained a majority on BCH, might reduce
the minimum relay fee in order to launch a transaction flooding attack
in order to disrupt the network, or to make mining unprofitable for
other miners.

Risk of flooding could be mitigated by other policy tools, and it may be
worth investigating whether to put such more advanced anti-flooding measures
in place before deploying this CHIP.

### Risk of miners not using the fee voting at all

Miners might just ignore the voting mechanism, ie. just signal for
"No Change" even in face of drastically rising real world BCH price.

### Risk of miners increasing fees beyond reasonable levels

This scenario applies only to an increase from an already low
absolute fee rate (e.g. a few sat / kB) while the price of BCH is
very high.

In that case, upward fee increases could quickly escalate to beyond
economical levels (a few dollars at least, in today's prices).

The author considers this is as unlikely based on historical evidence.
There is a diverse group of mining pools mining on BCH, each with their
own interests and profitability considerations. They can already govern
their own minimum relay fees and mining fees at this time, and form
cartels to coordinate, yet have not abused this fee-setting power so far.

As the network volume and the proportion of transaction fees in miner
revenue increases, the reaction by users to a fee hike would become more
pronounced, as the majority of users would try to economize on fees.
They would be alerted quickly to such increases, which would also be
indicated ahead of time on the network, allowing feedback and
reputational cost to miners who might want to squeeze the user base on
fees.

### Risk of someone somehow gaming the adjustment algorithm

The author finds it hard to see in which way miners could gain
from gaming the relay fees especially because manipulation would
be quite visible (evidence accumulating in blocks on the chain).

Bad relay performance or not mining transactions due to insufficient
fee rate would hurt economic activity, causing coin reputation and
price to suffer at least in the medium to long term.

Long range games might be possible with a week's voting period, such
as raising fees slightly ahead of periods with high expected transaction
volume, and then lowering them again.

### Risk of maximum adaptation rate not being sufficient

This refers to the risk of fee change not being able to keep up with
sudden price changes either up or down.

The result could be that some other coins are more economical to
transact on during some time until the BCH algorithm catches up.

### Risk of Overshoot / undershoot

This refers to miners (pools) not changing their fee voting back to
'No Change' on time, thus overshooting or undershooting their preferred
fee target.

This could well happen initially as hashpower learns and familiarizes
itself with the new policy lever. Additionally it will take some
time for everyone to evaluate the systemic effects of fee changes
(apart from the relief felt when fees are lowered, or the possible
anguish when they are raised).

A conceivable mitigation is to reduce the adjustment percentage, but this
comes at the cost of slower fee adjustments and should only be done
once the BCH price is less volatile.

Another mitigation could be that implementations allow the operator
to set a fee target and thereafter automatically adjusts their voting
according to the current fee policy

### Risk of Reduced Fee Rate Stability

The present, simple network wide policy has a big benefit - the minimum
relay fee is stable at 1000 sat/kB.

When this voting scheme is deployed, the effective (calculated) fee rate
would generally fluctuate, over longer periods of time, around a price
target sought by the hashpower in aggregate. This would occur as long as
the target fee rate sought by miners (according to the voting) lies
somewhere is not exactly the next step in the commonly desired direction.

The lower the effective fee rate already is, the less the magnitudes of
such fluctuations, however at the high end of the fee spectrum,
where jumps are bigger, this effect might be amplified.

A mitigating factor here would be that the fee adjustments are generally
anticipated to be of downward nature, and to become necessary only when
the market value of BCH has increased much.
This would indicate that the first relief sought would be a dramatic
downward fee rate correction (perhaps to 100 sat/kB or less).

Another mitigating factor could be built into implementations, to
rest at 'no change' if the operators fee rate target is approached to
within a suitable margin. This could be helpful in dampening such
fluctuations.


## Evaluation of Alternatives

There are many conceivable alternatives algorithms by which fee changes
may be enacted, driven by voting or otherwise.
At the time of writing none have been published as CHIPs, but some which
have been discussed are listed below.

### One time manual fee rate adjustment

It has been proposed to reduce the minimum fee to 50 sat/KB
using a block height activation at some time in the near future
(e.g. to activate on or before 15 November 2021).

This would be a "can kick" in the style of previous fee rate adjustments
performed historically by Bitcoin Core. The author has proposed this
CHIP as a way of shifting the discussion to a mechanism which can
serve for the longer future and would hopefully not require further
kicking of cans. Another drawback of such one-time fee changes
is that each time, they deduct resources from various development teams.
A more permanent solution that unburdens developers would allow them
to focus on the other pressing work to ensure that the Bitcoin Cash network
remains usable as it becomes used more heavily.

### Adjusting up or down linearly by N sats/kB

This has been proposed by another developer (with N = 1 sat/kB).
This seems too slow to cope adequately with sudden price changes,
unless the voting period is shrunk dramatically, in which case we
venture toward the less safe side of hash-based voting. A very short
voting period could also destabilize the fees as it becomes easier for
hostile hash to influence.

Overall, the argument against linear changes is that they are slow
unless big values of N is used. But big values of N also make big
changes on the low end of the fee scale. This does not seem attractive
to the author - fees should only be really low if the coin price is
already correspondingly high, in which case big adjustments have a
great impact. In the "couple of sats" range, we should mostly be needing
to make only small adjustments.


### Choosing a fee rate from miner-encoded absolute fee proposals

In such a scheme, miners would encode a preferred minimum fee rate
into the blocks somehow, e.g. as data in the coinbase transaction or
using more bits in the header.

The effective fee rate would be computed from this encoded data, again
based on some number of past blocks and some statistical method like
weighted averaging, taking the median value or some other kind of
interpolation.

Such a scheme could enable adjustments of greater magnitude than are
possible under this CHIP, and thus be able to react faster to greater
swings in the relative value of BCH.

It may also lead to greater variance in fee movements, be more
complicated to program correctly in other application (look at what
happened in the case of the relatively simple infamous median computation
of the DAA). The author also considers that such proposals could lead to
"voting for extreme values" in order to swing the average or arrive
at median / compromise solutions that would be statistically calculated
by such algorithms.

Overall, such algorithms will be much more complex, and potentially
harder to predict in terms of fee rate outcomes, than the simpler
algorithms.

The author does expect that some counter proposals to this CHIP may
suggest such algorithms, and would like to state that he is not
principally opposed to such solutions, but considers them more
complex than needed and recommends starting out with a simpler
solution, and only changing it if it proves unfit in time.


## Stakeholders and Statements

[TODO]

### Full node developers

### Major businesses

### Miners and pools

### Wallets and clients

### Application and Library Developers

### Users

[Statements to be collected.]


## Appendix A - Fee Projections

The table below gives some fee (in USD) projections at various USD/BCH prices
and a few illustrative fee rate between 1000..1 sat/kB.

These are based on a 500 bytes estimate for a typical transaction.

| BCH Price (USD/BCH) | Min          | fees        | at         | various    | rates      |            |            |
| ------------------- | ------------ | ----------- | ---------- | ---------- | ---------- | ---------- |  --------- |
|                     | 1000 sat/kB  | 500 sat/kB  | 100 sat/kB | 50 sat/kB  | 10 sat/kB  | 5 sat/kB   | 1 sat/kB   |
| $100.--             | $0.0005000   | $0.0002500  | $0.0000500 | $0.0000250 | $0.0000050 | $0.0000025 | $0.0000005 |
| $500.--             | $0.0025000   | $0.0012500  | $0.0002500 | $0.0001250 | $0.0000250 | $0.0000125 | $0.0000025 |
| $1,000.--           | $0.0050000   | $0.0025000  | $0.0005000 | $0.0002500 | $0.0000500 | $0.0000250 | $0.0000050 |
| $2,000.--           | $0.0100000   | $0.0050000  | $0.0010000 | $0.0005000 | $0.0001000 | $0.0000500 | $0.0000100 |
| $3,000.--           | $0.0150000   | $0.0075000  | $0.0015000 | $0.0007500 | $0.0001500 | $0.0000750 | $0.0000150 |
| $5,000.--           | $0.0250000   | $0.0125000  | $0.0025000 | $0.0012500 | $0.0002500 | $0.0001250 | $0.0000250 |
| $10,000.--          | $0.0500000   | $0.0250000  | $0.0050000 | $0.0025000 | $0.0005000 | $0.0002500 | $0.0000500 |
| $20,000.--          | $0.1000000   | $0.0500000  | $0.0100000 | $0.0050000 | $0.0010000 | $0.0005000 | $0.0001000 |
| $50,000.--          | $0.2500000   | $0.1250000  | $0.0250000 | $0.0125000 | $0.0025000 | $0.0012500 | $0.0002500 |
| $100,000.--         | $0.5000000   | $0.2500000  | $0.0500000 | $0.0250000 | $0.0050000 | $0.0025000 | $0.0005000 |
| $1,000,000.--       | $5.0000000   | $2.5000000  | $0.5000000 | $0.2500000 | $0.0500000 | $0.0250000 | $0.0050000 |
| $10,000,000.--      | $50.0000000  | $25.0000000 | $5.0000000 | $2.5000000 | $0.5000000 | $0.2500000 | $0.0500000 |


## Appendix B - Walk Distance Data and Size of Covered/Uncovered Ranges

This table shows, for various adjustment percentages considered,
how many of the fee values in the integer range [1,1000] are
reachable (covered) under the operations of incrementing or decrementing
a fee, and how many fee points the algorithm visits at a given percentage
during a straight-line walk from ceiling to floor or vice versa
(these walk distances include the endpoints).

| percentage | covered | uncovered | ceiling to floor | floor to ceiling |
| ---------- | ------- | --------- | ---------- | ---------- |
| 0.1        | 1000    | 0         | 1000       | 1000       |
| 0.25       | 900     | 100       | 899        | 900        |
| 0.3        | 834     | 166       | 832        | 834        |
| 0.5        | 1000    | 0         | 615        | 617        |
| 1          | 989     | 11        | 381        | 384        |
| 1.5        | 996     | 4         | 282        | 285        |
| 2          | 998     | 2         | 225        | 229        |
| 2.5        | 999     | 1         | 189        | 192        |
| 3          | 999     | 1         | 163        | 167        |
| 5          | 998     | 2         | 107        | 111        |
| 7          | 994     | 6         | 81         | 85         |
| 10         | 990     | 10        | 59         | 64         |
| 12.5       | 985     | 15        | 48         | 53         |
| 15         | 979     | 21        | 41         | 47         |
| 20         | 961     | 39        | 31         | 37         |
| 25         | 938     | 62        | 25         | 31         |
| 33.3       | 886     | 114       | 19         | 26         |
| 37.5       | 860     | 140       | 17         | 23         |
| 42         | 824     | 176       | 15         | 21         |
| 50         | 779     | 221       | 11         | 18         |


## Appendix C - Charts

The various charts that follow were used by the author in deciding on a percentage value
for the proposed fee adjustments.

Fee decrease curves for various adjustment percentage, starting from initial value of 1000 sat/kB:

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/curves_from_1000_down_smooth.svg">

Fee increase curves for various adjustment percentage, starting from initial value of 1 sat/kB (see Note below diagram):

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/curves_to_1000_up_smooth.svg">

(Note on the above chart: as the curves are computed simply using floating point exponentiation, they do not match up exactly with the integer math which also uses a minimum of 1 sat/kB in its steps. See further "step charts" for the various percentage values below)

Number of steps required to get from fee ceiling to fee floor and vice versa, for various adjustment percentages:

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/walk_distances.svg">

Number of integer values in the range [1,1000] which are not hit at various percentages.

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/cov-uncov-and-walk-steps.svg">


The fee values hit during walks up and down for the percentages. These are also computed using integer math taking into account the 1 sat/kB minimum step value.

p = 0.1%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-0.10.svg">

p = 0.25%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-0.25.svg">

p = 0.3%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-0.30.svg">

p = 0.5%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-0.50.svg">

p = 1.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-1.00.svg">

p = 1.5%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-1.50.svg">

p = 2.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-2.00.svg">

p = 2.5%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-2.50.svg">

p = 3.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-3.00.svg">

p = 5.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-5.00.svg">

p = 7.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-7.00.svg">

p = 10.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-10.00.svg">

p = 12.5%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-12.50.svg">

p = 15.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-15.00.svg">

p = 20.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-20.00.svg">

p = 25.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-25.00.svg">

p = 33.3%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-33.30.svg">

p = 37.5%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-37.50.svg">

p = 42.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-42.00.svg">

p = 50.0%

<img src="CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/steps-50.00.svg">


## Appendix D - Test data for 25%

These following test data / test vectors have been produced by the
[calculate_and_plot.py](CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/calculate_and_plot.py)
Python program.

The values for adjustment percentage = 25% have been extracted.

They are briefly discussed below.

The "walk down" sequence starts with fee = 1000 and does a
series of decrements until it hits the floor.
The initial value (1000) is counted as a "step" in this output,
but the "25 steps" should be interpreted merely as the number of
elements in this walk, including the floor and ceiling end points.

```
Walk down: 25 steps
[1000, 750, 563, 423, 318, 239, 180, 135, 102, 77, 58, 44, 33, 25, 19, 15,
 12, 9, 7, 6, 5, 4, 3, 2, 1]
```

The "walk up" sequence starts with fee = 1 and does a series of
increments until it hits the ceiling.

```
Walk up: 31 steps
[1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 15, 18, 22, 27, 33, 41, 51, 63, 78, 97,
 121, 151, 188, 235, 293, 366, 457, 571, 713, 891, 1000]
```

The "fee closure" is the set of all integer values in [1,1000] that can
be reached from floor or ceiling by applying an arbitrary sequence of fee
increases or decreases.
The set is given below for the adjustment percentage of 25%.
The "size" is the number of elements in the set.

```
Fee closure (size: 938)
 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
  21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
  39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
  57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
  75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
  93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123,
 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,
 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153,
 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168,
 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183,
 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198,
 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213,
 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228,
 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243,
 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258,
 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273,
 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288,
 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303,
 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318,
 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333,
 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348,
 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363,
 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378,
 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393,
 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408,
 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423,
 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438,
 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453,
 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468,
 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483,
 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498,
 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513,
 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528,
 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543,
 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558,
 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573,
 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588,
 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603,
 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618,
 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633,
 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648,
 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663,
 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678,
 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693,
 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708,
 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723,
 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738,
 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 750, 751, 752, 753, 755,
 756, 757, 758, 760, 761, 762, 763, 765, 766, 767, 768, 770, 771, 772, 773,
 775, 776, 777, 778, 780, 781, 782, 783, 785, 786, 787, 788, 790, 791, 792,
 793, 795, 796, 797, 798, 800, 801, 802, 803, 805, 806, 807, 808, 810, 811,
 812, 813, 815, 816, 817, 818, 820, 821, 822, 823, 825, 826, 827, 828, 830,
 831, 832, 833, 835, 836, 837, 838, 840, 841, 842, 843, 845, 846, 847, 848,
 850, 851, 852, 853, 855, 856, 857, 858, 860, 861, 862, 863, 865, 866, 867,
 868, 870, 871, 872, 873, 875, 876, 877, 878, 880, 881, 882, 883, 885, 886,
 887, 888, 890, 891, 892, 893, 895, 896, 897, 898, 900, 901, 902, 903, 905,
 906, 907, 908, 910, 911, 912, 913, 915, 916, 917, 918, 920, 921, 922, 923,
 925, 926, 927, 928, 930, 931, 932, 933, 935, 937, 938, 940, 941, 943, 945,
 946, 947, 950, 951, 952, 953, 956, 957, 958, 960, 962, 963, 965, 966, 968,
 970, 971, 972, 975, 976, 977, 978, 981, 982, 983, 985, 987, 988, 990, 991,
 993, 995, 996, 997, 1000}
```

"Values not hit" below is the difference between the entire range
(the set {1,2,...,1000}) and the closure set above.

It shows which values should never be reached for adjustment
percentage 25% (62 such fee values are "not covered" under this
fee adjustment percentage).

```
Values not hit (size: 62):
[749, 754, 759, 764, 769, 774, 779, 784, 789, 794, 799, 804, 809, 814, 819,
 824, 829, 834, 839, 844, 849, 854, 859, 864, 869, 874, 879, 884, 889, 894,
 899, 904, 909, 914, 919, 924, 929, 934, 936, 939, 942, 944, 948, 949, 954,
 955, 959, 961, 964, 967, 969, 973, 974, 979, 980, 984, 986, 989, 992, 994,
 998, 999]
```

If your implementation produces one of these values which it should
not hit, or visits different fee levels on the walks between ceiling and floor,
then there is something wrong!


## References

[1] [Version bits with timeout and delay (BIP 9)](https://github.com/bitcoin/bips/blob/master/bip-0009.mediawiki)

[2] [Version bits with lock-in by height (BIP 8)](https://github.com/bitcoin/bips/blob/master/bip-0008.mediawiki)

[3] [Stratum extension for version-rolling bit mask (BIP 310)](https://github.com/bitcoin/bips/blob/master/bip-0310.mediawiki)

[4] [Reserved nVersion bits for general purpose use (Draft BIP 661)](https://github.com/bitcoin/bips/pull/661/files)

[5] [Python code, SVG files etc](CHIP-2021-05-Minimum_Fee_Rate_Voting_Via_Versionbits/)

[6] [2015 bitcoin-dev mailing list post by Jeff Garzik](https://lists.linuxfoundation.org/pipermail/bitcoin-dev/2015-December/011973.html)


## Abbreviations used in this document

| Abbreviation |     Meaning                                |
| ------------ | ------------------------------------------ |
| ATH          | All Time High                              |
| ATL          | All Time Low                               |
| BCH          | Bitcoin Cash                               |
| BIP          | Bitcoin Improvement Proposal               |
| CHIP         | (Bitcoin) Cash Improvement Proposal        |
| FAB          | Fee Adjustment Bit                         |
| LSB          | Least Significant Bit                      |
| MTP          | Median Time Past                           |


## Acknowledgments

Roger Ver and Alexander Levin for their initial impetus to preventively
address fee escalation problems in Bitcoin Cash.

Jeff Garzik for inspiration from his December 2015 mailing list post about
fee markets, economic change events and developer moral hazard.

MaxHastings for suggesting an additional miner-related risk.

imaginary_username for suggesting a better human interface for
configuring the voting behavior in node software.

BigBlockIfTrue for recommending a change from supermajority
to simple majority for the threshold.


### CHIP Design & Implementation Process Timeline

2021-05-26 Started work on this CHIP

2021-05-30 Published initial draft v0.1

2021-06-06 Published draft v0.2 (updates from review, spelling corrections)


## Relay Minimum Fee Related Public Discussion Timeline

2021-05-15 [Initial thread on bitcoincashresearch.org](https://bitcoincashresearch.org/t/lower-the-default-relay-fee-create-a-fee-estimation-algorithm/440)

2021-05-22 [Reddit discussion thread initiated by Roger Ver](https://np.reddit.com/r/btc/comments/nig27h/it_is_already_time_to_implement_fractional/)

2021-05-30 [Reddit discussion thread following publication of this CHIP](https://np.reddit.com/r/btc/comments/no0atu/chip202105_minimum_fee_rate_voting_via_versionbits/)


## Copyright and Licensing

Copyright (C) 2021 freetrader

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

