#!/bin/sh

set -x

pandoc --pdf-engine=xelatex -V geometry:"top=2cm, bottom=1.5cm, left=2cm, right=2cm" CHIP-2021-05_Minimum_Fee_Rate_Voting_Via_Versionbits.md -o CHIP-2021-05_Minimum_Fee_Rate_Voting_Via_Versionbits.pdf
