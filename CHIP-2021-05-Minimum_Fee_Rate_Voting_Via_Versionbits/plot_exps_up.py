#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
from scipy import ndimage

PERCENTAGES = [0.1, 0.25, 0.3, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0, 7.0, 10.0, 12.5, 15.0, 20.0, 25.0, 33.3, 37.5, 42.0, 50.0]

t = np.arange(0., 80., 1.0)

plt.rc('figure', figsize=(16.53,11.69))  # A3 landscape
fig, ax = plt.subplots()
ax.grid(axis='both', which='both')
ax.xaxis.grid(True, which='minor')
ax.xaxis.set_minor_locator(MultipleLocator(4))
ax.yaxis.grid(True, which='minor')
ax.yaxis.set_minor_locator(MultipleLocator(10))
plt.xlim([0, 80])
plt.ylim([0, 1000])

plt.title('1*(1+p/100)^n (p = percentage)', color='#6b0eb2', fontsize='16', fontweight='bold')
plt.xlabel('n', fontsize='14', fontweight='bold')
plt.ylabel('Fee at nth step (sat/kB)', fontsize='14', fontweight='bold')

for p in PERCENTAGES:
    plt.plot(t, (1+p/100.0)**t, label="{}".format(p))

#plt.grid(axis='both', which='both')
plt.legend(loc=1)
plt.subplots_adjust(left=0.05,
                    bottom=0.08,
                    right=0.95,
                    top=0.95)
#plt.show()
plt.savefig('curves_to_1000_up_smooth.svg')
