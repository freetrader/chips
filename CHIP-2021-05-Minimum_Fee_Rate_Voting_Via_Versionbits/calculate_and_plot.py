#!/usr/bin/env python3
"""
Calculate the fee steps for a percentage-based adjustment proposal.

This program computes the fee increments/decrements both using
floating point and integer math, and asserts that the results
are identical, even under closure of increment/decrement operations.

All calculated fee adjustments are rounded up to provide at least
a 1 satoshi/kB increment or decrement.

Implementations should prefer integer arithmetic unless they check
that the set of fees computed by their implementation matches the
closure under increment/decrement as computed by this program.
You do NOT want your implementation to calculate a divergent
result from the rest of the network.

Some percentage values, like 35.0, do NOT yield identical closures
under float/int math and should not be considered for implementation.
"""

import matplotlib.pyplot as plt
import numpy as np

FEE_FLOOR   = 1     # sat / kB
FEE_CEILING = 1000  # sat / kB
# Not all the percentages below have an exact floating point representation.
PERCENTAGES = [0.1, 0.25, 0.3, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0, 7.0, 10.0, 12.5, 15.0, 20.0, 25.0, 33.3, 37.5, 42.0, 50.0]
# For verification of integer math, we use the above percentage * 100
INTEGER_PERCENTAGES = [n * 100 for n in PERCENTAGES]
# The number of percentages to evaluate, based on the list above.
NUM_PERCENTAGES = len(PERCENTAGES)
# Sanity checks that we constructed the arrays correctly
assert(NUM_PERCENTAGES == len(INTEGER_PERCENTAGES))
assert([x*100 for x in PERCENTAGES] == INTEGER_PERCENTAGES)


def float_increment_fee(current_fee, percentage):
    """ The percentage parameter is a float from PERCENTAGES. """
    assert(percentage > 0.0 and percentage < 100.0)
    if (current_fee == FEE_CEILING):
        return FEE_CEILING
    else:
        increment = max(1, int(current_fee * (percentage / 100)))
        assert(increment >= 1)
        return min(FEE_CEILING, current_fee + increment)


def float_decrement_fee(current_fee, percentage):
    """ The percentage parameter is a float from PERCENTAGES. """
    assert(percentage > 0.0 and percentage < 100.0)
    if (current_fee == FEE_FLOOR):
        return FEE_FLOOR
    else:
        decrement = max(1, int(current_fee * (percentage / 100)))
        assert(decrement >= 1)
        return max(FEE_FLOOR, current_fee - decrement)


def integer_increment_fee(current_fee, percentage):
    """ The percentage parameter is an integer from INTEGER_PERCENTAGES. """
    assert(percentage > 0 and percentage < 10000)
    if (current_fee == FEE_CEILING):
        return FEE_CEILING
    else:
        # check floor division == trunc division (all positive fee values)
        assert ((current_fee * percentage) // 10000) == int((current_fee * percentage) / 10000)
        increment = max(1, int((current_fee * percentage) / 10000))
        assert(increment >= 1)
        return min(FEE_CEILING, current_fee + increment)


def integer_decrement_fee(current_fee, percentage):
    """ The percentage parameter is an integer from INTEGER_PERCENTAGES. """
    assert(percentage > 0 and percentage < 10000)
    if (current_fee == FEE_FLOOR):
        return FEE_FLOOR
    else:
        # check floor division == trunc division (all positive fee values)
        assert ((current_fee * percentage) // 10000) == int((current_fee * percentage) / 10000)
        decrement = max(1, int((current_fee * percentage) / 10000))
        assert(decrement >= 1)
        return max(FEE_FLOOR, current_fee - decrement)


def fee_closure(initial_fee_set, increment_operation, decrement_operation):
    result_set = initial_fee_set
    while True:
        found_new = False
        new_result_set = result_set.copy()
        for elem in result_set:
            inc_elem = increment_operation(elem)
            dec_elem = decrement_operation(elem)
            if inc_elem not in result_set:
                new_result_set.add(inc_elem)
                found_new = True
            if dec_elem not in result_set:
                new_result_set.add(dec_elem)
                found_new = True
        result_set = new_result_set
        if not found_new:
            break
    return result_set

collected_data = dict()

for percentage_index in range(NUM_PERCENTAGES):
    percentage = PERCENTAGES[percentage_index]
    percentage_int = INTEGER_PERCENTAGES[percentage_index]
    assert(100*percentage == percentage_int)
    print("Adjustment percentage:", percentage) 
    collected_data[percentage] = dict()

    starting_value = FEE_CEILING
    for initial_fee in [FEE_CEILING,]:
        # do a decrement walk down to FEE_FLOOR,
        # then an increment walk up to FEE_CEILING
        # then an walk back down to FEE_FLOOR (to check if identical)

        # walk down to floor
        decrement_steps = []  # to collect the values
        decrement_steps.append(initial_fee)
        new_fee = float_decrement_fee(initial_fee, percentage)
        new_fee_int = integer_decrement_fee(initial_fee, percentage_int)
        assert new_fee == new_fee_int, "from initial: {} new fee (float math) {} does not match new fee (int math) {}".format(initial_fee, new_fee, new_fee_int)
        while (new_fee > FEE_FLOOR):
            decrement_steps.append(new_fee)
            old_fee = new_fee
            new_fee = float_decrement_fee(old_fee, percentage)
            new_fee_int = integer_decrement_fee(old_fee, percentage_int)
            assert new_fee == new_fee_int, "from previous: {} new fee (float math) {} does not match new fee (int math) {}".format(decrement_steps[-1], new_fee, new_fee_int)
        assert(new_fee == FEE_FLOOR)
        decrement_steps.append(FEE_FLOOR)

        # walk up to ceiling
        # collect the decrementing fee range
        increment_steps = []
        while (new_fee < FEE_CEILING):
            increment_steps.append(new_fee)
            old_fee = new_fee
            new_fee = float_increment_fee(old_fee, percentage)
            new_fee_int = integer_increment_fee(old_fee, percentage_int)
            assert new_fee == new_fee_int, "new fee (float math) {} does not match new fee (int math) {}".format(new_fee, new_fee_int)
        assert(new_fee == FEE_CEILING)
        increment_steps.append(FEE_CEILING)

        print("Walk down: {} steps".format(len(decrement_steps)))
        print(decrement_steps)
        print("Walk up: {} steps".format(len(increment_steps)))
        print(increment_steps)

        fc_float = fee_closure(set(increment_steps) | set(decrement_steps),
                         lambda x: float_increment_fee(x, percentage),
                         lambda y: float_decrement_fee(y, percentage))
        fc_int = fee_closure(set(increment_steps) | set(decrement_steps),
                             lambda x: integer_increment_fee(x, percentage_int),
                             lambda y: integer_decrement_fee(y, percentage_int))
        assert fc_float == fc_int, "fee closure do not match (float vs integer math) not in float: {} not in int: {}".format(fc_int - fc_float, fc_float - fc_int)
        # Check that some key fractions of present-day fee level are in the closure
        # This is for possibly selecting a safe higher initial floor which
        # could be lowered later on.
        # 1/2, 1/4, 1/5, 1/10, 1/20, 1/40, 1/50, 1/100, 1/200
        levels_to_check = set([5, 10, 20, 25, 50, 100, 200, 250, 500])
        assert levels_to_check.issubset(fc_float), "levels not in closure: {}".format(levels_to_check - set(fc_float))
        # Unfortunately they are not all in the up/down walk sequences.
        # But it does not matter.
        #assert levels_to_check.issubset(increment_steps), "levels not in increment walk: {}".format(levels_to_check - set(increment_steps))
        #assert levels_to_check.issubset(decrement_steps), "levels not in decrement walk: {}".format(levels_to_check - set(decrement_steps))
        print("Fee closure (size: {})\n".format(len(fc_float)), fc_float)
        whole_range = range(FEE_FLOOR, FEE_CEILING + 1)
        values_not_hit = sorted(set(whole_range) - fc_float)
        print("Values not hit (size: {}): {}".format(len(values_not_hit), values_not_hit))
        print("Summary: percentage: {} "
              "ceiling-to-floor: {} "
              "floor-to-ceiling: {} "
              "range: {} "
              "covered: {} "
              "not covered: {}".format(percentage, len(decrement_steps), len(increment_steps), len(whole_range), len(fc_float), len(values_not_hit)))

        collected_data[percentage]['ctf'] = len(decrement_steps)
        collected_data[percentage]['ftc'] = len(increment_steps)
        collected_data[percentage]['range'] = len(whole_range)
        collected_data[percentage]['cov'] = len(fc_float)
        collected_data[percentage]['uncov'] = len(values_not_hit)

        # Create a figure with 2 subplots, showing steps for each walk direction
        steps_down = np.asarray(decrement_steps)
        steps_up = np.asarray(increment_steps)

        plt.rc('figure', figsize=(16.53,11.69))  # A3 landscape
        plt.figure(1 + percentage_index)

        plt.subplot(211, label='subplot 1')
        subplt_axes = plt.subplot(211).axes
        plt.title("Downward steps for adjustment percentage = {}%".format(percentage))
        plt.xlabel('Step Number')
        plt.ylabel('Fee (sat/kB)')
        x = np.array(range(len(decrement_steps)))
        plt.grid(axis='both', which='both')
        plt.stem(x, steps_down, linefmt='r-', markerfmt=' ', basefmt="gray", use_line_collection=True)

        plt.subplot(212, label='subplot 2')
        subplt_axes = plt.subplot(212).axes
        plt.title("Upward steps for adjustment percentage = {}%".format(percentage))
        plt.xlabel('Step Number')
        plt.ylabel('Fee (sat/kB)')
        x = np.array(range(len(increment_steps)))
        plt.grid(axis='both', which='both')
        plt.stem(x, steps_up, linefmt='g-', markerfmt=' ', basefmt="gray", use_line_collection=True)
        plt.subplots_adjust(left=0.07,
                            bottom=0.1,
                            right=0.95,
                            top=0.95,
                            hspace=0.5)
        plt.savefig("steps-%.2f.svg" % percentage)

# Prepare arrays for plotting
p = np.asarray(PERCENTAGES)
cov = np.asarray([collected_data[i]['cov'] for i in p])
uncov = np.asarray([collected_data[i]['uncov'] for i in p])
ctf = np.asarray([collected_data[i]['ctf'] for i in p])
ftc = np.asarray([collected_data[i]['ftc'] for i in p])

print("percentage,covered,uncovered,cft_steps,ftc_steps")
for i in range(len(p)):
    print("{},{},{},{},{}".format(p[i], cov[i], uncov[i], ctf[i], ftc[i]))

# Create a figure with 4 stem-plots, one for each of the above (cov, uncov, ctf, ftc).
plt.figure(NUM_PERCENTAGES + 1)

plt.subplot(221, label='Fig 1')
subplt_axes = plt.subplot(221).axes
plt.title('points covered in range')
plt.xlabel('adjustment percentage')
plt.ylabel('n')
plt.xscale('log')
subplt_axes.set_xticks(PERCENTAGES)
subplt_axes.set_xticklabels(PERCENTAGES, rotation=(90))
plt.stem(p, cov, linefmt='r-', markerfmt=' ', basefmt="gray", use_line_collection=True)

plt.subplot(222, label='Fig 2')
subplt_axes = plt.subplot(222).axes
plt.title('points not covered in range')
plt.xlabel('adjustment percentage')
plt.ylabel('n')
plt.xscale('log')
subplt_axes.set_xticks(PERCENTAGES)
subplt_axes.set_xticklabels(PERCENTAGES, rotation=(90))
plt.stem(p, uncov, linefmt='r-', markerfmt=' ', basefmt="gray", use_line_collection=True)

plt.subplot(223, label='Fig 3')
subplt_axes = plt.subplot(223).axes
plt.title('number of steps for ceiling-to-floor')
plt.xlabel('adjustment percentage')
plt.ylabel('n')
plt.xscale('log')
subplt_axes.set_xticks(PERCENTAGES)
subplt_axes.set_xticklabels(PERCENTAGES, rotation=(90))
plt.stem(p, ctf, linefmt='r-', markerfmt=' ', basefmt="gray", use_line_collection=True)

plt.subplot(224, label='Fig 4')
subplt_axes = plt.subplot(224).axes
plt.title('number of steps for floor-to-ceiling')
plt.xlabel('adjustment percentage')
plt.ylabel('n')
plt.xscale('log')
subplt_axes.set_xticks(PERCENTAGES)
subplt_axes.set_xticklabels(PERCENTAGES, rotation=(90))
plt.stem(p, ftc, linefmt='r-', markerfmt=' ', basefmt="gray", use_line_collection=True)
plt.subplots_adjust(left=0.05,
                    bottom=0.08,
                    right=0.95,
                    top=0.95,
                    wspace=0.2,
                    hspace=0.5)
plt.savefig("cov-uncov-and-walk-steps.svg", dpi=600)


# Plot walk distances on a bar chart, with up/down on same graph
x = np.array(range(NUM_PERCENTAGES))
y = ctf
y2 = ftc
plt.figure(NUM_PERCENTAGES + 2)
width = 0.3
plt.bar(x, y, color='b', width=width, label='ceiling-to-floor', edgecolor='k')
plt.bar(x+width, y2, color='c', width=width, label='floor-to-ceiling', edgecolor='k')
plt.title('Walk distances (up/down)', color='#6b0eb2', fontsize='16', fontweight='bold')
plt.xlabel('Adjustment percentages', fontsize='14', fontweight='bold')
plt.ylabel('Number of steps required', fontsize='14', fontweight='bold')
plt.xlim(xmin=0, xmax=NUM_PERCENTAGES)
plt.ylim(ymin=1, ymax=1000)
plt.yscale('log')
subjects = ["%.2f%%" % p for p in PERCENTAGES]
plt.xticks(x+width/2, subjects, rotation=60)
plt.legend(loc=1)
plt.subplots_adjust(bottom=0.1, top=0.95, left=0.05, right=0.95)
plt.savefig("walk_distances.svg", dpi=600)
