# CHIP-2023-06-Commit_Size_Of_Block

        Title: Commit Size of Block
        Maintainer: freetrader
        Authors: freetrader (freetrader@tuta.io)
        Type: Technical
        Is consensus change: Yes
        Status: DRAFT
        Version: 0.1
        Initial Publication Date: 2023-06-15
        Last Revision Date: 2023-06-15

## Summary

This CHIP introduces a commitment to the byte size of a block as an additional
data item in the coinbase transaction (following the BIP34 block height).

## Terms and Conventions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in RFC 2119.

## Background

Traditionally, while there has been a commitment to a block's height within
the coinbase transaction (BIP34), the block's size has remained an unknown
number which could only be verified by attempting to download the entire
block.

This left open an attack vector where a malicious peer could offer a valid
proof of work header, but supply excessive and invalid network data for
the actual block content. The downloader would only find out that the
block was invalid after attempting to download as much data as could
possibly fit in a block (32MB on BCH at the time of writing this CHIP,
but potentially much more in future).

This has been recognized as an unfortunate circumstance for a long time
by Bitcoin and Bitcoin Cash developers.

With on-chain commitments on the horizon for both UTXO fast-sync (TODO:
reference to CHIP) and adaptive block size algorithms (TODO: reference to CHIP),
an opportunity presents itself to introduce a simple commitment for the
size of a block.

## Motivation and Benefits

The main motivation and benefit of this CHIP is as follows:

- Software that validates blocks, can limit itself to downloading only up
  to the committed number of bytes, instead of having to potentially download
  as much data as the current blocksize limit (in whatever way it is
  determined) would prescribe.

Side benefits that may be realized in conjunction with other developments:

- Algorithms that determine the size limit of the next block could use this
  committed information as part of their calculus

- Algorithms that do pruning could use this information to optimize their
  storage use

- Mining software could use this new committed data point to expedite its
  own decision whether to build on a block or not, potentially saving the
  time to download the rest of the block

## Limitation of Scope

Considered to be **within the scope** of this CHIP are:

- adding a commitment for the actual block size
- adding requirements to validate a block's size against such a commitment
  and inform the user through an error if such validation fails

Considered to be **outside of the scope** of this CHIP are:

- introducing commitments for other purposes
- proposing a general extensible scheme for arbitrary commitments
- determining the exact user-facing presentation of block size commitment
  data and validation errors (this is largely left in the purview of software
  that implements the functionality)

## Specification

### REQ-BLOCKSIZE-COMMITMENT-FIELD

The block size (in bytes) of the current block SHALL be added as the second item
in the coinbase transaction's scriptSig (after the BIP34 block height field).

### REQ-BLOCKSIZE-COMMITMENT-FORMAT

The format of the block size commitment SHALL be:

- "minimally encoded serialized CScript":
  first byte is number of bytes in the number, following bytes are little-endian representation of the number (including a sign bit)

Note 1: The format is the same as for the height field.

### REQ-BLOCKSIZE-COMMITMENT-LIMIT

The block size commitment value SHALL be considered invalid if it exceeds
the excessive block size limit (aka the hard limit) applicable to the
block in question, regardless of how that hard limit is computed.

Note 1: In simple terms, the commitment value shall never exceed the hard
        block size limit.

Note 2: The current hard limit is typically set by the "excessive blocksize"
        configuration item on full node software and defaults to 32MB at
        the time of writing.
        However, this limit is subject to network consensus, and can be
        adapted in configuration.
        It may also be computed dynamically in the future for incoming blocks
        using an adaptive algorithm (TODO: CHIP reference).

### REQ-BLOCKSIZE-COMMITMENT-VALIDATION

A block SHALL be considered invalid if either

- its commitment value is invalid as per REQ-BLOCKSIZE-COMMITMENT-LIMIT
- its actual size does not equal its committed block size value

### REQ-BLOCKSIZE-COMMITMENT-VALIDATION-ERROR

If a block fails validity checking due to a mismatching block size commitment,
an error SHALL be raised to notify of the mismatch between actual block size
and committed block size.

Note 1: The code or message of the error is not prescribed, but it SHOULD
        allow the user to determine that there was a discrepancy to
        the committed block size value.

## Deployment

Deployment of this CHIP is proposed for the May 2024 BCH upgrade,
in the usual manner of BCH upgrades
(MTP-based upgrade, with 6-month earlier activation on chipnet).


## Backward compatibility

Full nodes would need to upgrade or risk accepting blocks which violate
the new block size commitment validity requirement.

Older software that does not upgrade would (incorrectly) interpret the new field
as part of the coinbase message which to date has followed directly after the
BIP34 block height field.

## Test Cases

TODO

## Implementation Costs and Risks

### Costs

Full nodes and libraries that validate block data, would need to be modified
to validate this commitment for blocks following the activation of this proposal.

Software which generates coinbase transactions for mining would need
to be modified to include the size of the proposed block.

Software which parses coinbase transaction inputs (e.g. block explorers)
would need to be modified to decode the block size field.

SPV servers would not need to be modified.

Wallets likewise need no changes.

### Risks

#### Forcing users to shorten their coinbase messages

The coinbase input script is used by miners and pools to store coinbase
messages.

The size of the script is limited to TODO bytes. There is a risk that
some users are already at the limit, and adding another commitment to the
script may force these users to shorten their coinbase messages.

TODO: obtain data on coinbase message in use on the blockchain to quantify

#### Obsoleting unadaptable software that cannot accomodate the commitment

There is a risk that other software used to generate blocks, cannot be
adapted to include the new commitment in the coinbase transaction.

However, this would likely apply then to any commitment scheme, whether
it uses the coinbase transaction or other method.

### Ongoing Costs and Risks

This proposal does not significantly increase ongoing operating costs.

The addition of the block size commitment involves no additional
computations since the block size is already being calculated when
blocks are generated (in order to comply with excessive block size
constraints).

The ongoing maintenance costs are similarly expected to be low,
like for the BIP34 block height field.

## Evaluation of Alternatives

### Not adding the block size commitment

Work would still be needed to add commitments for UTXO fast sync and a block
size hash for an adaptive algorithm, if these were to enter into consensus,
so the work of adding commitments _somewhere_ is not avoidable.

Not adding the current block size would only save a minimal amount of effort
and data, but lose the main benefit of being able to better control the
network download of block data.

### Adding commitment outside of coinbase

To add a block size (or other commitment) without using the coinbase,
would need to introduce another transaction to store the commitment.

This adds more complexity as such a transaction would need to be:

- funded (either consuming an output of the coinbase or needing some other UTXO)
- identifiable as being for the purposes of the commitment
- efficiently locatable (otherwise it defeats the purpose of introducing it)

Since there is still sufficient room in the coinbase for commitments (see
data TODO), imposing this additional complexity seems a worse choice.

## Security considerations

Once committed, the size of a block cannot simply be altered as the entire
block, including the Merkle hash of all its transactions, is covered by proof of work.
Downloading only the committed number of bytes of the block payload
allows validation of the commitment.

A malicious peer could generate a valid header with an excessive
block size commitment, but that commitment could not exceed the excessive block
size limit in Bitcoin Cash. As such there is no risk of a network peer being
tricked into downloading more data than it may already.
Misbehaving peers that serve bad commitments would be subject to the existing
treatment for serving invalid blocks (usually: banning such peers).

## Stakeholders

Stakeholders are mostly developers of software that interprets the coinbase
transaction input script, as they will need to adapt to the addition of
commitments like this.

This includes:

- full nodes
- block explorers (although most do only limited interpretation of the field)
- libraries that offer block validation

## Statements responses and statements

TODO

## Implementations

TODO

## References

TODO

## Abbreviations used in this document

| Abbreviation |     Meaning                                |
| ------------ | ------------------------------------------ |
| BCH          | Bitcoin Cash                               |
| BIP          | Bitcoin Improvement Proposal               |
| CHIP         | (Bitcoin) Cash Improvement Proposal        |
| MTP          | Median Time Past                           |
| UTXO         | Unspent Transaction Output                 |

## Acknowledgments

Thanks to the following people who have contributed insights that helped
with this proposal:

bitcoincashautist, imaginary_username

### CHIP timeline

2023-06-14 Started work on this CHIP

2021-06-15 Published initial draft v0.1

## Copyright and Licensing

Copyright (C) 2023 freetrader

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
